//
//  AppDelegate.h
//  webview
//
//  Created by SHOKI TAKEDA on 7/25/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

